# Listener

Listener displays a helpful smiley that you can talk to without being judged.
For less cheerful topics, you can change the smiley to look sad.

To programmers, Listener even provides a Rubber Ducky mode that forces you to explain your 
bug in simple terms, making it easier to understand what the actual problem is.

<div align="center">
  <img src="data/screenshots/happy.png">
</div>

## Installation

The currently supported method of installation is via Flathub:

<a href='https://flathub.org/apps/details/io.gitlab.gregorni.Listener'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

## Development

The easiest way to work on this project is by cloning it with GNOME Builder:

1. Install and open [GNOME Builder](https://flathub.org/apps/details/org.gnome.Builder)
2. Select "Clone Repository..."
3. Clone `https://gitlab.com/gregorni/Listener.git` (or your fork)
4. Run the project with the ▶ button at the top, or by pressing `Ctrl`+`Shift`+`Space`.

## Code of Conduct

This project follows the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct)

